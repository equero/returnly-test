export class API {
  static async getProduct() {    
    return fetch('./scripts/product.json')
      .then(res => res.json());    
  }
}