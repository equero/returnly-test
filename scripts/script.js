'use strict';
import { API } from './api.mock.js';
import { Helpers } from './helpers.js'

const scrollCausesElement = document.querySelector('.scroll-causes');
const returnableProductElement = document.querySelector('.returnable-product');

(async ()=> {
  const product = await API.getProduct().then(res => res.product);  
  const productDetailElement = document.querySelector('product-details');
  const productImageElement = productDetailElement.querySelector('.product-img');
  const imageChild = productImageElement.querySelector('img');
  const productTitleElement = productDetailElement.querySelector('.product-title-price');
  const titleChild = productTitleElement.querySelector('h4');
  const priceChild = productTitleElement.querySelector('.product-price');
  const productReturnDetailsElement = productDetailElement.querySelector('.product-return-details');
  const returnDetailsChild = productReturnDetailsElement.querySelector('.return-details');
  const colorChild = returnDetailsChild.querySelector('p');
  const orderNumberChild = returnDetailsChild.querySelector('.product-order-number');
  const smallPriceChild = productDetailElement.querySelector('.small-price-container');
    
  Helpers.setAttributes(imageChild, { 
    src: product.image.src,
    srcset: product.image.src,
    alt: product.image.alt
  });

  titleChild.innerHTML = product.title;
  priceChild.innerHTML = `$${product.variants[0].price}`;
  colorChild.innerHTML = product.variants[0].color ? `Color: ${product.variants[0].color}` : 'Color: Unknown'
  orderNumberChild.innerHTML = `Order: ${product.variants[0].id}`;
  smallPriceChild.innerHTML = `$${product.variants[0].price}`; 
})();

const checkSelectedState = (causes) => {
  let existChecked = false;  
  for (let x = 0; x < causes.length; x++) {    
    if (!causes[x].hasAttribute('hidden')) {
      existChecked = true;
      x = causes.length;
    }
  }

  if (existChecked) {
    returnableProductElement.classList.add('selected');
  } else {
    returnableProductElement.classList.remove('selected');
  }
};

const cleanCheckedOptions = (causes) => {
  for (let x = 0; x < causes.length; x++) {
    if (!causes[x].hasAttribute('hidden')) {
      causes[x].setAttribute('hidden', '');
    }
  }
}

scrollCausesElement.addEventListener('click', event => {
  if (event.target.matches('label') || event.target.matches('span')) {
    const productDetailElement = document.querySelector('.return-cause');
    const causes = productDetailElement.getElementsByClassName('fa fa-check');
    const targetIcon = event.target.parentElement.querySelector('i');
    if (!targetIcon.hasAttribute('hidden')) {
      targetIcon.setAttribute('hidden', '');
    } else {      
      cleanCheckedOptions(causes);
      if (targetIcon.hasAttribute('hidden')) {
        targetIcon.removeAttribute('hidden');
        returnableProductElement.classList.remove('activated-returnable-product');
        event.stopPropagation();
      } else {
        targetIcon.setAttribute('hidden', '');
      }            
    }    
    checkSelectedState(causes);
  }  
});

returnableProductElement.addEventListener('click', event => {    
    event.currentTarget.classList.toggle('activated-returnable-product');
});
