const API_URL = 'https://frontend-code-test.myshopify.com/admin/api/2019-10/products/4579368370235.json';

export class API {
  static async fetchProduct() {
    const headers = new Headers();
    headers.append('X-Shopify-Access-Token', 'c42073762cfb6bedd59f8cb9483b8b1a');    
    return fetch(API_URL, {...headers}).then(res => res.json());
  }
}