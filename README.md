# What's on my mind while I'm taking the test 

I have to admit that it has been difficult to decide which direction to take on the test. On the one hand it looks simple and with many basic concepts at stake. My first impression is to think that the idea is to know if the candidate has an ease to understand Hoisting, Event Bubbling, Scope Prototype, Dom Manipulation, strict mode, how browser works, Fetch Api and some CSS. 

But looking at the HTML I could perceive that there were some ng ( Angular) tags around, so maybe the idea was to make an angular app to develop this test. Honestly my final verdict is that angular is too overkill for a simplified version of the web return system, where on top of that the resources are not correctly enabled with CORS.

## Testing
Just use `npx start`

## Review of all points

**Please fill product card with the information that each element requires.**
An extremely easy task with any modern framework, but one that is made difficult to perform with javascript vanilla. One point to note is that the shopify product does not belong to any purchase order so some data is missing. Also, when I fetch the product I get a CORS error, so I created two files, a normal api with real requests and a mock to develop.

**Replace the way we display return causes from hover state to clickable so it goes**
The key to perform this task is a correct use of the delegation of events since the number of causes is dynamic and could be 10 or a thousand.

**Finally customize it as one of our merchant Untuckit [https://www.untuckit.com/](https://www.untuckit.com/).**
I must admit that this task has been the most complicated for me since I am not a great designer, most of my work experience has been concentrated in a fullstack environment, where the design and layout belonged to someone else.

I have been working hard on the layout to keep up with the new design patterns, like BEM architecture.

Another key point is to modify an existing css by creating intermediate files.

